from contextlib import asynccontextmanager
from typing import Sequence

from sqlalchemy import select, text, update
from sqlalchemy.engine.row import RowMapping
from sqlalchemy.ext.asyncio import (
    AsyncAttrs,
    AsyncEngine,
    AsyncSession,
    create_async_engine,
)
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from fastapi import FastAPI


@asynccontextmanager
async def lifespan(global_app: FastAPI):
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    await engine.dispose()


app: FastAPI = FastAPI(lifespan=lifespan)
engine: AsyncEngine = create_async_engine("sqlite+aiosqlite:///recipes.db", echo=True)


class Base(AsyncAttrs, DeclarativeBase): ...


class Recipes(Base):
    __tablename__ = 'recipes'

    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str]
    cooking_time: Mapped[int]
    ingredients: Mapped[str]
    description: Mapped[str]
    count_view: Mapped[int] = mapped_column(server_default=text('1'))


@app.get('/')
async def all_recipes():
    async with AsyncSession(engine) as session:
        recipes: Sequence[RowMapping] = (
            (
                await session.execute(
                    select(
                        Recipes.title, Recipes.count_view, Recipes.cooking_time
                    ).order_by(Recipes.count_view.desc(), Recipes.cooking_time)
                )
            )
            .mappings()
            .all()
        )

    return recipes


@app.get('/recipes/{recipe_id}')
async def get_recipe(recipe_id: int):
    async with AsyncSession(engine) as session:
        recipe: RowMapping | None = (
            (
                await session.execute(
                    select(
                        Recipes.title,
                        Recipes.cooking_time,
                        Recipes.ingredients,
                        Recipes.description,
                    ).where(Recipes.id == recipe_id)
                )
            )
            .mappings()
            .fetchone()
        )

        if recipe:
            await session.execute(
                update(Recipes)
                .where(Recipes.id == recipe_id)
                .values(count_view=Recipes.count_view + 1)
            )

            await session.commit()

    return recipe


@app.get('/create_recipes')
async def create_recipes():
    async with AsyncSession(engine) as session:
        recipe1: Recipes = Recipes(
            title='Суп «Харчо»',
            cooking_time=60,
            ingredients='Курица бройлерная - 1 шт. Рис - 0,5 стакана...',
            description='Суп харчо - вкусный, ароматный, острый. '
            'Традиционно харчо варят из говядины, но по этому рецепту '
            'готовится суп с курицей.',
        )

        recipe2: Recipes = Recipes(
            title='Куриный суп с вермишелью',
            cooking_time=45,
            ingredients='Филе куриное - 2 шт. (500 г). Картофель - 2-3 шт...',
            description='Куриный суп с вермишелью с удовольствием съедят и дети, '
            'и взрослые. Рецепт супа из куриного филе очень прост.',
        )

        recipe3: Recipes = Recipes(
            title='Тосканский суп с фаршем',
            cooking_time=40,
            ingredients='Фарш свиной или говяжий - 250 г Картофель - 300 г (3 шт.)...',
            description='Разнообразить меню первых блюд трудно, мы привыкли '
            'готовить и есть одни и те же супы. Этот тосканский '
            'суп с фаршем вдохновит вас пересмотреть своё меню.',
        )

        recipe4: Recipes = Recipes(
            title='Томатный суп с курицей, фасолью и овощами',
            cooking_time=41,
            ingredients='Куриное филе - 300-350 г Фасоль консервированная - 200 г',
            description='Суп с курицей, консервированной фасолью, '
            'помидорами в собственном соку и свежим сладким перцем.',
        )

        session.add_all([recipe1, recipe2, recipe3, recipe4])
        await session.commit()
